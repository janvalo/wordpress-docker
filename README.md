# dev-setup

Docker setup for a Wordpress website development

## Requirements

- docker
- docker-compose


## Run

    clone this repo

    cd into the folder

    run docker-compose up

    you can access the site then using http://localhost:8080

I would recommend to set your hosts file for the domain ```websitename.local``` for easier access


## Issues

create issue for this repo if there are any problems